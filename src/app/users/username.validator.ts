import { Validators, AbstractControl } from '@angular/forms';

export class UserNameValidator {
    static notContainSpace(control: AbstractControl) {
        if ((control.value as string).indexOf(' ') !== -1) {
            return { spaceNotAllow: true };
        } else {
            return null;
        }
    }

    static AsyncCall(control: AbstractControl): Promise<Validators | null> {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                if (control.value === 'raj') {
                    resolve({ matchName: true });
                } else {
                    resolve();
                }
            }, 2000);
        });
    }
}
