import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { UserNameValidator } from './username.validator';
import { trigger, state, style, animate, transition, keyframes } from '@angular/animations';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  animations: [
    trigger('fade', [
      transition('void => *', [
        style({ opacity: 0 }),
        animate(2000)
      ])
    ])
  ]
})

export class UsersComponent implements OnInit {
  form: any;
  @Output() addUser = new EventEmitter<any>();
  emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.form = new FormGroup({
      username: new FormControl('',
        [Validators.required, Validators.minLength(3), UserNameValidator.notContainSpace],
        UserNameValidator.AsyncCall),
      email: new FormControl('', [Validators.required, Validators.pattern(this.emailPattern)])
    });
  }
  get username() {
    return this.form.get('username');
  }

  get email() {
    return this.form.get('email');
  }

  clickForm() {
    this.addUser.emit({
      userName: this.username.value,
      email: this.email.value
    });
  }

}
