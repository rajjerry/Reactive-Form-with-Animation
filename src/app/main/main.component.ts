import { Component, OnInit } from '@angular/core';
import { trigger, state, style, animate, transition, keyframes } from '@angular/animations';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
  animations: [
    trigger('flyInOut', [
      transition('void => *', [
        animate(900, keyframes([
          style({ opacity: 0, transform: 'translateX(-100%)', offset: 0 }),
          style({ opacity: 1, transform: 'translateX(30px)', offset: 0.3 }),
          style({ opacity: 1, transform: 'translateX(0)', offset: 1.0 })
        ]))
      ]),
      transition('* => void', [
        animate(900, keyframes([
          style({ opacity: 1, transform: 'translateX(0)', offset: 0 }),
          style({ opacity: 1, transform: 'translateX(-30px)', offset: 0.7 }),
          style({ opacity: 0, transform: 'translateX(100%)', offset: 1.0 })
        ]))
      ])
    ])
  ]
})

/* Query and stagger is used for delay animations. Like one box after another so on.
  In Animation we have callbacks if animation started we can call animation.
start callback function and animations end callback function.
*/
 export class MainComponent implements OnInit {

  constructor() { }
  newUser = false;
  users: any;

  ngOnInit() {
    this.users = [{ email: 'jariwalaraj04@gmail.com', userName: 'raj' }];
    this.users.push({ email: 'rajjerry04@gmail.com', userName: 'rajjerry' },
      { email: 'abc@gmail.com', userName: 'rajRossi' });
  }

  createNewUser() {
    this.newUser = !this.newUser;
  }

  addEventHandlerUser(event: any) {
    this.users.push({
      email: event.email,
      userName: event.userName
    });
    this.newUser = !this.newUser;
  }

  remove(index) {
    this.users.splice(index, 1);
  }
}
